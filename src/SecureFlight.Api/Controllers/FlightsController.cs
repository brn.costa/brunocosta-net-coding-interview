﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using SecureFlight.Core.Services;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(FlightService flightService, IService<Passenger> passengerService, IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPut("{flightId:long}/{passengerId}")]
    [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> AddPassengersByFlight(long flightId, string passengerId)
    {
        var passenger = await passengerService.FindAsync(passengerId);
        if(passenger is null)
        {
            return NotFound("Passenger not found!");
        }

        var flight = await flightService.FindAsync(flightId);
        if (flight is null)
        {
            return NotFound("Flight not found!");
        }

        var result = await flightService.AddPassengerByFlight(flight, passenger);

        if (!result.Succeeded)
            return BadRequest(result.ErrorResult);

        return Ok(result);
    }
}