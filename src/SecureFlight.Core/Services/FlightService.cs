﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class FlightService(IRepository<Flight> repository, IRepository<PassengerFlight> passengerFlightRepo) : BaseService<Flight>(repository)
    {
        public async Task<OperationResult<PassengerFlight>> AddPassengerByFlight(Flight flight, Passenger passenger)
        {
            PassengerFlight passengerFlight = new()
            {
                FlightId = flight.Id,
                PassengerId = passenger.Id,
            };

            var addResult = await passengerFlightRepo.Add(passengerFlight);

            return addResult is null ?
                OperationResult<PassengerFlight>.Error($"Failed to add the passenger to flight {flight.Id}") :
                OperationResult<PassengerFlight>.Success(addResult);
        }
    }
}
